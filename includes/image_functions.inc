<?php

/**
 * This file contains custom image-processing functions.
 */

// Output image

function media_image_transform_image_output(stdClass $image) {
  return image_toolkit_invoke('media_image_transform_output', $image);
}

function image_gd_media_image_transform_output(stdClass $image) {
  header('Content-type: image/jpeg');
  imagejpeg($image->resource);
  // Clear any image generation messages from being displayed
  drupal_get_messages();
  exit;
}

// Rotate image

function media_image_transform_image_rotate(stdClass $image, $rotate) {
  return image_toolkit_invoke('media_image_transform_rotate', $image, array('rotate' => $rotate));
}

function image_gd_media_image_transform_rotate(stdClass $image, $rotate) {
  $rotate = (string) $rotate;
  switch ($rotate) {
    case '180':
      $image->resource = imagerotate($image->resource, 180, 0);
      break;
    case '90cw':
      $image->resource = imagerotate($image->resource, -90, 0);
      break;
    case '90ccw':
      $image->resource = imagerotate($image->resource, 90, 0);
      break;
  }
  return $image;
}

// Flip image horizontal

function media_image_transform_image_flip_horizontal(stdClass $image) {
  return image_toolkit_invoke('media_image_transform_flip_horizontal', $image);
}

function image_gd_media_image_transform_flip_horizontal(stdClass $image) {
  $im = $image->resource;
  $sx = imagesx($im);
  $sy = imagesy($im);
  $temp = imagecreatetruecolor($sx, $sy);
  $result = imagecopyresampled($temp, $im, 0, 0, ($sx-1), 0, $sx, $sy, (0-$sx), $sy);
  if ($result) {
    $image->resource = $temp;
    $temp = null;
  }
  return $image;
}

// Flip image vertical

function media_image_transform_image_flip_vertical(stdClass $image) {
  return image_toolkit_invoke('media_image_transform_flip_vertical', $image);
}

function image_gd_media_image_transform_flip_vertical(stdClass $image) {
  $im = $image->resource;
  $sx = imagesx($im);
  $sy = imagesy($im);
  $temp = imagecreatetruecolor($sx, $sy);
  $result = imagecopyresampled($temp, $im, 0, 0, 0, ($sy-1), $sx, $sy, $sx, (0-$sy));
  if ($result) {
    $image->resource = $temp;
    $temp = null;
  }
  return $image;
}