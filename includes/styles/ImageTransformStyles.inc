<?php

class ImageTransformStyles extends FileStyles {
    
  function thumbnail($effect) {
    parent::thumbnail($effect);
    // Check if transform overrides are set
    /*
    if (!is_null($this->override('transform'))) {
      $this->apply_transformations($effect);
    } else {
      parent::thumbnail($effect);
    }
    */
  }
  
  function applyTransformations($effect) {
    // Temp
    parent::thumbnail($effect);
  }
  
  function getImageUri() {
    // Alter the URL for the WYSIWYG browser
    // TODO: Add permissions check here
    $uri = parent::getImageUri();
    if ($this->override('transform')) {
      //if ($this->override('wysiwyg')) {
        // Replace this with an admin-restricted preview
        $uri = 'imagetransform://' . $this->getFid() . '/transform/' . urlencode(http_build_query($this->override('transform')));
      //}
    }
    return $uri;
  }
  
  public function render($reset = FALSE) {
    $return = parent::render($reset);
    return $return;
  }
  
}