<?php

/**
 * Media Image Transform's (imagetransform://) stream wrapper class.
 *
 */
class ImageTransformStreamWrapper extends DrupalLocalStreamWrapper {
  public function getDirectoryPath() {
    return 'admin/media_image_transform/preview';
  }
  public function getExternalUrl() {
    $parts = explode('/', $this->getTarget());
    $data = array();
    for ($i=0; $i<count($parts); $i=$i+2) {
      $data[$parts[$i]] = urldecode($parts[$i+1]);
    }
    return url($this->getDirectoryPath(), array('query' => $data));
  }
}