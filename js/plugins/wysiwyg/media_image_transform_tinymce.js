// TODO: Abstract editor-specific functions, unbind/preserve elements after hiding tooltip

(function ($) {

Drupal.wysiwyg.plugins.media_image_transform = {
  
  ed: false,
  qtip: false,
  crop: false,
  currentTarget: false,

  attach: function(content, settings, instanceId) {
    var self = this;
    this.initializeOverlay();
    // TODO: move changes from wysiwyg-media.js to here
    this.setEditor(tinyMCE.get(instanceId));
    this.getEditor().onClick.add(function(ed, evt){
      self.attachImageButtons(ed, evt);
    });
    return content;
  },
  
  initializeOverlay: function() {
    var self = this;
    self.qtip = $('<div />').qtip({
      id: 'mit-overlay',
      content: {
        text: 'test',
        title: {
          text: 'Image Transform Properties',
          button: true
        }
      },
      position: {
        my: 'center',
        at: 'center',
        target: $(window)
      },
      style: {
        classes: 'ui-tooltip-dark ui-tooltip-shadow ui-tooltip-rounded'
      },
      show: {
        event: 'click',
        solo: true,
        modal: true
      },
      events: {
        show: function(evt, api) {
          self.initializeOverlayContent(evt, api);
        },
        hide: function(evt, api) {
          api.set('content.text', '');
        },
      },
      hide: false
    });
  },
  
  /**
   * Builds the HTML for the overlay
   */
  initializeOverlayContent: function(evt, api) {
    var self = this;
    var ct = self.currentTarget;
    var params = $.deparam(ct.src.substr(ct.src.indexOf('?') + 1));
    params.style = '';
    var styleRe = /\bimg__view_mode__([^\s]+)\b/ig;
    var styleMatches = styleRe.exec(ct.className);
    if (styleMatches[1]) {
      params.style = styleMatches[1];
    }
    if (typeof(params.imagetransform) != 'undefined') {
      params.fid = params.imagetransform;
      delete params.imagetransform;
    } else {
      var styleRe = /\bimg__fid__(\d+)\b/ig;
      var styleMatches = styleRe.exec(ct.className);
      if (styleMatches[1]) {
        params.fid = styleMatches[1];
      }
    }
    if (typeof(params.transform) == 'string') {
      params.transform = $.deparam(params.transform);
    } else if (typeof(params.transform) == 'undefined') {
      params.transform = {};
    }
    
    if (typeof(params.transform.flip) != 'object') {
      params.transform.flip = [];
    }
    if (typeof(params.transform.rotate) == 'undefined') {
      params.transform.rotate = '0';
    }
    var globalDiv = $('<div />').attr('id', 'mit-global');
    var lcolDiv = $('<div />').attr('id', 'mit-lcol').appendTo(globalDiv);
    var rcolDiv = $('<div />').attr('id', 'mit-rcol').appendTo(globalDiv);
    var section = false;
    // Left column
    section = $('<div />').addClass('mit-section section-cropping').appendTo(lcolDiv);
    $('<h3 />').text('Cropping').appendTo(section);
    var previewDiv = $('<div />').attr('id', 'mit-preview').appendTo(section);
    // Right column
    // Flip canvas
    section = $('<div />').addClass('mit-section section-flip').appendTo(rcolDiv);
    $('<h3 />').text('Flip Canvas').appendTo(section);
    var flip = $('<div />').attr('id', 'mit-flip-buttonset').append(
      $('<input />').attr({type: 'checkbox', id: 'flip-h'}),
      $('<label />').attr('for', 'flip-h').text('Flip horizontal'),
      $('<input />').attr({type: 'checkbox', id: 'flip-v'}),
      $('<label />').attr('for', 'flip-v').text('Flip vertical')
    ).appendTo(section);
    section = $('<div />').addClass('mit-section section-rotation').appendTo(rcolDiv);
    $('<h3 />').text('Rotation').appendTo(section);
    var rotate = $('<div />').attr('id', 'mit-rotate-buttonset').append(
      $('<input />').attr({type: 'radio', id: 'rotate-0', name: 'rotate', value: '0'}),
      $('<label />').attr('for', 'rotate-0').text('None'),
      $('<input />').attr({type: 'radio', id: 'rotate-180', name: 'rotate', value: '180'}),
      $('<label />').attr('for', 'rotate-180').html('180&deg;'),
      $('<input />').attr({type: 'radio', id: 'rotate-90cw', name: 'rotate', value: '90cw'}),
      $('<label />').attr('for', 'rotate-90cw').html('90&deg; CW'),
      $('<input />').attr({type: 'radio', id: 'rotate-90ccw', name: 'rotate', value: '90ccw'}),
      $('<label />').attr('for', 'rotate-90ccw').html('90&deg; CCW')
    ).appendTo(section);
    // Rotate
    // Buttons
    var buttonsDiv = $('<div />').attr('id', 'mit-buttons').appendTo(globalDiv);
    buttonsDiv.append($('<button />').attr('id', 'mit-button-save').text('Save changes'));
    buttonsDiv.append($('<button />').attr('id', 'mit-button-cancel').text('Cancel'));
    api.set('content.text', globalDiv);
    // UI actions
    // Button actions
    $('#mit-button-save').button({icons: {primary: 'ui-icon-circle-check'}}).click(function() {
      // Removed unused params
      if (params.transform.flip.length == 0) {
        delete params.transform.flip;
      }
      if (params.transform.rotate.toString() == '0') {
        delete params.transform.rotate;
      }
      var crop = self.crop;
      params.transform.crop = {
        x: crop[0],
        y: crop[1],
        w: crop[2]-crop[0],
        h: crop[3]-crop[1],
      };
      var imgParams = {
        imagetransform: params.fid,
        transform: params.transform
      };
      var ed = tinyMCE.activeEditor;
      var newSrc = self.getPreviewUrl() + '?' + $.param(imgParams);
      ed.dom.setAttrib(ct.id, 'src', newSrc);
      api.hide();
    });
    $('#mit-button-cancel').button({icons: {primary: 'ui-icon-circle-close'}}).click(function() {
      api.hide();
    });
    // Flip actions
    $('#flip-h').button({icons: {primary: 'ui-icon-arrowthick-2-e-w'}}).click(function() {
      if ($.inArray('h', params.transform.flip) > -1) { // Remove from array
        params.transform.flip = self.removeValueFromArray(
          params.transform.flip, 'h'
        );
      } else {
      	if (typeof(params.transform.flip) == 'undefined') {
      		params.transform.flip = ['h'];
      	} else {
	        params.transform.flip.push('h');
        }
      }
      self.onFormChange(params);
    });
    $('#flip-v').button({icons: {primary: 'ui-icon-arrowthick-2-n-s'}}).click(function() {
      if ($.inArray('v', params.transform.flip) > -1) { // Remove from array
        params.transform.flip = self.removeValueFromArray(
          params.transform.flip, 'v'
        );
      } else {
      	if (typeof(params.transform.flip) == 'undefined') {
      		params.transform.flip = [];
      	}
        params.transform.flip.push('v');
      }
      self.onFormChange(params);
    });
    // Rotate actions
    $('#mit-rotate-buttonset input').click(function() {
      if ($(this).is(':checked')) {
        params.transform.rotate = $(this).val();
      }
      self.onFormChange(params);
    });
    $('#mit-flip-buttonset, #mit-rotate-buttonset').buttonset();
    self.onFormChange(params);
  },
  
  /**
   * Default event handler when any element in the form is changed
   */
  onFormChange: function(params) {
    var transform = params.transform;
    // Refresh preview
    this.updatePreview(params);
    // Reflect changes on form
    // Flip
    ($.inArray('h', transform.flip) > -1) ? $('#flip-h').attr('checked', 'checked') : $('#flip-h').removeAttr('checked');
    ($.inArray('v', transform.flip) > -1) ? $('#flip-v').attr('checked', 'checked') : $('#flip-v').removeAttr('checked');
    // Rotation
    if (transform.rotate) {
      switch (transform.rotate) {
        case '180':
        case '90cw':
        case '90ccw':
          $('#rotate-' + transform.rotate).attr('checked', 'checked');
          break;
        default:
          $('#rotate-0').attr('checked', 'checked');
          break;
      }
    } else {
      $('#rotate-0').attr('checked', 'checked');
    }
    $('#mit-flip-buttonset, #mit-rotate-buttonset').buttonset('refresh');
  },
  
  removeValueFromArray: function(arr, value) {
    var index = arr.indexOf(value);
    if (index != -1) {
      arr.splice(index, 1);
    }
    return arr;
  },
  
  getPreviewUrl: function() {
    return Drupal.settings.basePath + 'admin/media_image_transform/preview';
  },
  
  updatePreview: function(params) {
    var self = this;
    var previewDiv = $('#mit-preview');
    previewDiv.empty();
    var cropImg = $('<img />').attr({
      id: 'cropbox',
      src: self.getPreviewUrl() + '?' + $.param({
        imagetransform: params.fid,
        style: params.style,
        transform: params.transform,
        ignoreCrop: 1
      })
    });
    previewDiv.append(cropImg);
    self.crop = false;
    if (params.transform.crop) {
      var dims = params.transform.crop;
      if ((parseInt(dims.w) + parseInt(dims.h)) > 10) {
        self.crop = [
          parseInt(dims.x), 
          parseInt(dims.y), 
          parseInt(dims.x)+parseInt(dims.w), 
          parseInt(dims.y)+parseInt(dims.h)
        ];
      }
    }
    var cropParams = {
      boxWidth: 500, 
      boxHeight: 400, 
      onChange: function(c) {
        self.crop = [
          c.x, c.y, c.x2, c.y2
        ];
      }
    };
    if (self.crop != false) {
      cropParams.setSelect = self.crop;
    }
    $('#cropbox').Jcrop(cropParams);
  },

  /**
   * Attaches overlays to image elements
   * @static
   */  
  attachImageButtons: function(ed, evt) {
    var self = this;
    var dom = ed.dom;
    var targetImg = false;
    // Firefox
    if (evt.explicitOriginalTarget){
      if (evt.explicitOriginalTarget.nodeName.toLowerCase() == 'img'){
        targetImg = evt.explicitOriginalTarget;
      }
    }
    // IE
    else if (evt.target) {
      if (evt.target.nodeName.toLowerCase() == 'img'){
        targetImg = evt.target;
      }
    }
    // Provide an ID so we can reference this image if no ID is available
    if (targetImg.id == '') {
      dom.setAttrib(targetImg, 'id', dom.uniqueId('mit_img_'));
    }
    
    // TODO: Restrict valid targetImg to images that have the class media-image
    
    if (targetImg.id != 'mit_img_btn_edit') {
      Drupal.wysiwyg.plugins.media_image_transform.currentTarget = targetImg;
    
      // Remove any old buttons since this will be called repeatedly
      dom.remove('mit_img_btns');
      // Add button container
      dom.add(ed.getBody(), 'div', {
        id: 'mit_img_btns',
        class: 'mit_interface_element'
      })
      var editBtn = dom.add('mit_img_btns', 'img', {
        src: self.getAssetPath() + '/images/btn-edit-img.png',
        id: 'mit_img_btn_edit',
        width: '24',
        height: '24'
      });
      if (editBtn.contentEditable) {
      	editBtn.contentEditable = false;
      }
      tinyMCE.dom.Event.add(editBtn, 'click', function(e_evt) {
        var ct = self.currentTarget;
        var qtip = self.qtip;
        qtip.qtip('api').show();
        e_evt.stopPropagation();
      });
      
      if (targetImg) {
        var imgPos = dom.getRect(targetImg);
        dom.setStyles('mit_img_btns', {
          display: 'block',
          position: 'absolute',
          cursor: 'pointer',
          top: imgPos.y + 10,
          left: imgPos.x + 10
        });
      } else {
        self.qtip.qtip('api').hide();
      }
    
    }
  },
  
  /**
   * Gets the path to media assets needed for this plugin
   * @static
   */
  getAssetPath: function() {
    return Drupal.settings.media_image_transform.assetPath;
  },

  detach: function(content, settings, instanceId) {
    var contentObj = $('<div>' + content + '</div>');
    contentObj.find('.mit_interface_element').remove();
    return contentObj.html();
  },
  
  setEditor: function(ed) {
    this.ed = ed;
  },
  
  getEditor: function() {
    return this.ed;
  }

};

})(jQuery);
