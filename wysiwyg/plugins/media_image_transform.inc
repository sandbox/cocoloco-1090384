<?php

function media_image_transform_media_image_transform_plugin() {
  $plugins['media_image_transform'] = array(
    'title' => t('Media image transform'),
    'js path' => drupal_get_path('module', 'media_image_transform') . '/js/plugins/wysiwyg',
    'js file' => 'media_image_transform_tinymce.js',
    'settings' => array(),
    'icon file' => NULL,
    'icon title' => NULL,
    'icon path' => NULL,
    'dialog path' => NULL,
  );
  return $plugins;
}
